# DNS router and docker container monitoring daemon

There exists a github project of the same name, but for the most part this system has access to the docker instance, and is listening for new containers.  If the container contains the environment variable for virtual hosting, then dns requests will be routed to that host.